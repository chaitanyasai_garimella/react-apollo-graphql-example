'use strict';
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var TasksSchema = new Schema({
    taskName: String,
    isDelete: Boolean

});

module.exports = mongoose.model('Tasks', TasksSchema);
