//Please add all the resolvers at this file for now
var {PubSub}  =  require('graphql-subscriptions');
const TASK_ADDED = 'addedTask';
var Tasks = require("../model/tasks.model.js");
var Users = require("../model/users.model.js");
var pubsub = new PubSub();

const resolvers = {
    Query: {
        multipleTodos: (root) => {

            var foundItems = new Promise((resolve, reject) => {
                //var projections = getProjection(fieldASTs);
                Tasks.find((err, todos) => {
                    console.log(err,todos);
                    err ? reject(err) : resolve(todos)
                }).exec()
            });
            return foundItems;

        },
        singleTodo: (root,{taskName}) => {

            var foundItems = new Promise((resolve, reject) => {
                //var projections = getProjection(fieldASTs);
                Tasks.find({taskName},(err, todos) => {
                    console.log(err,todos);
                    err ? reject(err) : resolve(todos)
                }).exec()
            });

            return foundItems;

        }
    },
    Mutation: {
        addUser: (root,params) => {
            var taskData = new Tasks(params.newTodo);
            pubsub.publish(TASK_ADDED, taskData)

            return taskData.save();
        },
        updateUser:(root,params)=>{

            return Tasks.findByIdAndUpdate(params.id,{$set:params.updatedTodo});
        },
        deleteUser: (root,params) => {
            return Tasks.findByIdAndRemove(params.id).exec();
        },
        createUser: async (root, params) => {
            var res = {};
            var total_users = await Users.find().count();
            var userData = new Users({email: params.email, password: params.password, token: params.token,
            id: total_users+1});
            await userData.save(function (err, data) {
                if (err)
                    return err;

                res = data;
            });
            return res;
        }
    },

    Subscription: {
        addedTask: {
            subscribe:() =>  pubsub.asyncIterator(TASK_ADDED),
            resolve: (payload) => {
                console.log('The payload is ::: ', payload);
                return payload;
            }
        },
    }
};

export default resolvers;