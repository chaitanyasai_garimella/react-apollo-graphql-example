import React from 'react';
import  ReactDOM from 'react-dom';
import App from '../components/App/App.js'
import InsertTask from '../components/tasks/insertTask.js'
import { Router,Route,Switch} from 'react-router-dom';
import MainLayout from "../components/layout/layout.js";
/*
const MatchWithMainLayout = ({ exactly, pattern, component: Component }: any) => {
    return (
        <Match exactly={exactly} pattern={pattern} render={(props: any) => (
            <MainLayout><Component {...props} /></MainLayout>
        )} />
    );
};*/
const Routes = (props) => (
    <Switch props={props}>
        <MainLayout>
            <Route exact  path='/' render={() =>(<App {...props} />)} />
            <Route path="/addTask"  render={()=>(<InsertTask {...props} />)} />
        </MainLayout>


    </Switch>
);

export default Routes;
